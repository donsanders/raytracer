//////// Raytracer.cc begins here
/******************************************************************************
 Simple raytracer exercise.

 To practice boost, stl, c++ stdlib, Google C++ coding guidelines, libpng,
 and of course raytracing ;)

 On Linux build with:
   g++ -c -O2 raytracer.cc `libpng-config --cflags`
   g++ -o raytracer raytracer.o `libpng-config --ldflags` -lpthread

 Requires:
   libpng++-dev boost

 Notes:
   Google C++ coding guidelines allow only limited use of boost libraries.
   I've not adhered to this guideline as I want to experiment with using
   boost and modern C++ programming techniques, e.g. using smart pointers,
   and making code exception safe.

   Currently there is no separation of interface and implementation.
   Addressing this deficiency is a logical next step. Vector and Color
   typedefs could be promoted to full classes Vector, Color, Ray, Texture,
   Camera, Primitive, Sphere, Scene and helper functions could have
   handle/body (pimpl) idiom applied, and moved into separate
   header/implementation files.

   I'm not relying on return value optimization, e.g. on texture, background
   and unitNormal functions as surprisingly it adds about a 5% overhead, on
   both gcc and msvc on optimized/release builds.

   Additionally in violation of Google guidlines I use exceptions, as 
   a purpose of this exercise is to explore advanced C++ programming
   language features..
 *****************************************************************************/

#include <cassert>
#include <ctime>
#include <iostream>
#include <string>
#include <stdexcept> // Not allowed by guidelines see exception note above
#define FUTURE // C++11 thread support, not allowed by guidelines
#ifdef FUTURE
#include <future>
#include <vector>
#include <boost/bind.hpp> // For futures with member functions
#include <boost/function.hpp>
#endif
#include <png++/png.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
// Following includes are discouraged by Google guidelines see Note above.
#include <boost/shared_ptr.hpp> // Relatively lightly discouraged.
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/variate_generator.hpp>

// Use boost numeric::ublas library for vector algebra.
using namespace boost::numeric;

namespace {

// Placeholder function for internationlization of strings.
std::string i18n(const std::string &string) {
  return string;
}

// Three dimensonal vector.
typedef ublas::vector<double> Vector; // (x, y, z)

// RGB color triplet.
typedef ublas::vector<double> Color; // (r, g, b)

// Convenience function to create a 3 dimensional vector.
Vector create_vector(double x, double y, double z) {
  Vector result(3);
  result[0] = x;
  result[1] = y;
  result[2] = z;
  return result;
}

// Convenience function to create a color.
Color create_color(double r, double g, double b) {
  return create_vector(r, g, b);
}

// Constant use to prevent self collision, rounding errors, and division
// by zero.
// Must be a small value compared to vector values used in the scene,
// but std::numeric_limits<double>::min() is much too small.
const double kEpsilon = 1.0/1000.0;

class bad_scene : public std::runtime_error {
 public:
   explicit bad_scene(const std::string &what_arg) :runtime_error(what_arg) {};
  // Defaults function definitions provided are ok.
};

// Check to ensure a double is non-zero.
// If double is zero throws std::runtime_error and outputs error to std::cerr.
void check_zero(double d, const std::string &error) {
  if (std::abs(d) < kEpsilon) {
    throw bad_scene(error);
  }
}

// Cross product of 3 dimensional vectors, not included in ublas.
Vector cross_product(Vector a, Vector b) {
  Vector result(3);
  result[0] = a[1]*b[2] - a[2]*b[1];
  result[1] = a[2]*b[0] - a[0]*b[2];
  result[2] = a[0]*b[1] - a[1]*b[0];
  return result;
}

// Ray consists of an origin vector and a direction vector.
struct Ray {
  // No default constructor required currently.
  Ray(Vector a_origin, Vector a_direction);
  Vector origin;
  Vector direction;
};

Ray::Ray(Vector a_origin, Vector a_direction) {
  origin = a_origin;
  direction = a_direction;
}

// Texture of a point on an object consists of color, reflectiveness,
// absorbtion constants for ambient, diffuse and specular light,
// phongAlpha constant for shininess of phong highlights.
struct Texture {
  // Simple value class, default constructor etc provided and used.
  Color color;
  double reflectiveness; // Range is 0.0 to 1.0, 1.0 is completely reflective.
  double ka; // Ambient light absorbtion rate.
  double kd; // Diffuse light absorbtion rate.
  double ks; // Specular light absorbtion rate.
  double phongAlpha; // Phong specular highlight rate.
};

// Function to return texture given at point.
typedef void (*TextureFunction)(const Vector &point, Texture *texture);

// Function to return background color at a point.
// Direction must be a non-zero vector.
typedef void (*BackgroundFunction)(const Vector &direction, Color *color);

// Abstract base class (ABC) for objects in the scene.
class Primitive {
 public:
  // Explicit constructor as not a conversion operator.
  explicit Primitive(TextureFunction texture_function);
  // No default constructor provided as this class is an ABC.
  virtual ~Primitive() {};
  // Ray direction must not be zero.
  virtual bool intersect(const Ray &ray, Vector *intersection) const = 0;
  void texture(const Vector &point, Texture *texture) const;
  virtual void unitNormal(const Vector &point, Vector *normal) const = 0;

 private:
  TextureFunction texture_function_;
};

Primitive::Primitive(TextureFunction texture_function) {
  texture_function_ = texture_function;
}

void Primitive::texture(const Vector &point, Texture *texture) const {
  (*texture_function_)(point, texture);
}

// Camera settings used to render the scene.
struct Camera {
  // No default constructor required currently.
  Camera(Vector a_origin, Vector a_focus, Vector up_hint,
         int a_size, double a_scale,
         int a_super_samples, int a_jitter_samples);
  Vector origin; // Origin point of camera.
  Vector focus; // Point camera is looking at.
  int size; // Vertical size in pixels aspect, x/y ratio is 4/3.
  double scale; // Vertical size of viewport in world coordinates ratio is 4/3.
  int super_samples; // Square root of number of samples per pixel.
  int jitter_samples; // Square root of number of jitter rays per super sample.
                      // 0 means use 1 non-jittered ray per sample.
  // Derived variables.
  Vector direction;
  Vector up;
  Vector left;
  int x_size;
  int y_size;
  Vector x_increment;
  Vector y_increment;
  Vector top_left;
};

// Construct a camera,
// up_hint is a vector used to determine up direction for camera
//   up direction is intersection of the line through the points 'focus'
//   and 'origin + up_hint' and the plane defined by the point 'origin' on
//   the plane and normal vector to the plane 'focus - origin'.
Camera::Camera(Vector a_origin, Vector a_focus, Vector up_hint,
               int a_size, double a_scale,
               int a_super_samples, int a_jitter_samples) {
  origin = a_origin;
  focus = a_focus;
  direction = focus - origin;

  // Find camera up (and then left is cross product of up and direction).
  {
    // This is a bit complex, have to find intersection of a plane and line,
    // plane is '(p - p0)*n = 0', line is 'p = (d*l + l0)',
    // d and 0 scalars, p0, n, lo, l and the intersection point p are vectors.
    // See http://en.wikipedia.org/wiki/Line-plane_intersection Algebraic form.
    const std::string error(i18n("Camera origin, focus and up_hint "
                                 "must be distinct points."));
    Vector p0 = origin;
    Vector n = direction;
    Vector l0 = origin + up_hint;
    Vector l = l0 - focus;
    check_zero(inner_prod(l, n), error);
    const double d = inner_prod(p0 - l0, n)/inner_prod(l, n);
    up = (d*l + l0) - origin;
    left = cross_product(up, n);
    check_zero(norm_2(up), error);
    check_zero(norm_2(left), error);
    up *= 1/norm_2(up);
    left *= 1/norm_2(left);
  }

  { // Image and viewport sizes.
    check_zero(a_size, i18n("Image size must not be zero."));
    check_zero(a_size - 1, i18n("Image size must not be greater than one."));
    check_zero(a_scale, i18n("Image scale must not be zero."));
    size = a_size;
    scale = a_scale;
    x_size = size*4/3;
    y_size = size;

    const double x_scale = scale*4/3;
    const double y_scale = scale;
    x_increment = -left*x_scale/double(x_size-1);
    y_increment = -up*y_scale/double(y_size-1);
    top_left = focus + left*x_scale/2.0 + up*y_scale/2.0;
  }

  // Super sampling
  super_samples = a_super_samples;
  jitter_samples = a_jitter_samples;
}

typedef boost::ptr_vector<Primitive> Objects;
typedef boost::shared_ptr<Objects> ObjectsPtr;
typedef std::vector<Vector> Lights;
typedef boost::shared_ptr<Lights> LightsPtr;
typedef boost::shared_ptr<Camera> CameraPtr;

// Scene specifies background color function, objects and lights in scene, and
// the camera.
class Scene {
 public:
  // No default constructor
  Scene(BackgroundFunction background_function,
        ObjectsPtr objects,
        LightsPtr lights,
        CameraPtr camera,
        int max_depth = 10);
  void render();
  void write(const std::string &filename);

 private:
  // Non copyable class (no DISALLOW_COPY_AND_ASSIGN macro available).
  Scene(const Scene&);
  Scene& operator=(const Scene &);

  void render(int start_line, int end_line);
  void intersect(const Ray &ray,
                 Objects::const_iterator *object,
                 Vector *point) const;
  void illuminate(const Vector &point,
                  const Vector &normal,
                  const Texture &t,
                  Color *color) const;
  void cast_ray(const Ray &ray, int depth, Color *color) const;
  void background_color(const Vector &direction, Color *color) const;

  // Better to move this function out of Scene class than make it public,
  // See Myers prefer non-member non-friend functions.
  static png::rgb_pixel colorToPixel(const Color &c);

  BackgroundFunction background_function_;
  ObjectsPtr objects_;
  LightsPtr lights_;
  CameraPtr camera_;
  const int max_depth_;
  png::image< png::rgb_pixel > image_;
};

Scene::Scene(BackgroundFunction background_function,
             ObjectsPtr objects,
             LightsPtr lights,
             CameraPtr camera,
             int max_depth)
  :background_function_(background_function),
   objects_(objects),
   lights_(lights),
   camera_(camera),
   max_depth_(max_depth),
   // Possibly large memory allocation of image
   image_(camera_->x_size, camera_->y_size) {
}

void Scene::background_color(const Vector &direction, Color *result) const {
  (*background_function_)(direction, result);
}

// Computes intersection of \a ray with closest object in scene.
// Sets \a object to the closest object and \point to the point of intersection.
void Scene::intersect(const Ray &ray,
                      Objects::const_iterator *object,
                      Vector *point) const {
  Vector intersection;
  Vector nearest_intersection;
  double lowest_distance = 0.0;
  Objects::const_iterator i = objects_->begin();
  Objects::const_iterator j = objects_->end();
  while (i != objects_->end()) {
    if (i->intersect(ray, &intersection)) {
      Vector a = intersection - camera_->origin;
      double distance = inner_prod(a, a);
      if ((j == objects_->end()) || (distance < lowest_distance)) {
        lowest_distance = distance;
        j = i;
        nearest_intersection = intersection;
      }
    }
    ++i;
  }
  *object = j;
  *point = nearest_intersection;
}

void Scene::illuminate(const Vector &point,
              const Vector &normal,
              const Texture &t,
              Color *color) const {
  *color = t.ka*t.color;

  // Using phong model http://en.wikipedia.org/wiki/Phong_reflection_model.
  Lights::const_iterator i = lights_->begin();
  while (i != lights_->end()) {
    Ray l(point, *i - point);
    const double l_magnitude(norm_2(l.direction));
    if (l_magnitude < kEpsilon) {
      // Special case, collision between light and object.
      // Treat this as the object is self shadowed from light source.
      ++i;
      continue;
    }
    l.direction /= l_magnitude;
    l.origin += l.direction*kEpsilon;

    // Test for point being shadowed from light *i.
    Objects::const_iterator j;
    Vector intersection;
    intersect(l, &j, &intersection);
    if (j == objects_->end()) {
      // Point is visible to light *i, so it is not in the shade.
      double lambert = inner_prod(normal, l.direction);
      double phong = 0.0;
      if (lambert < 0.0) {
        lambert = 0.0;
      } else {
        // From wikipedia phone model page Rm = 2*(l*n)*n - l.
        Vector rm = 2*inner_prod(l.direction, normal)*normal - l.direction;
        const double rm_magnitude = norm_2(rm);
        if (rm_magnitude > 0.0) {
          rm /= rm_magnitude;
          Vector v = camera_->origin - point;
          const double v_magnitude(norm_2(v));
          // Avoid division by zero, when camera position equals object point.
          if (v_magnitude > 0.0) {
            v /= v_magnitude;
            phong = std::max(inner_prod(rm, v), 0.0);
            phong = pow(phong, t.phongAlpha);
          }
        }
      }
      *color += t.kd*t.color*lambert + t.ks*t.color*phong;
    }
    ++i;
  }
}

void Scene::cast_ray(const Ray &ray, int depth, Color *color) const {
  Objects::const_iterator j;
  Vector intersection;
  intersect(ray, &j, &intersection);
  if (j == objects_->end()) {
    // Ray missed objects, so hits the sky.
    background_color(ray.direction, color);
  } else {
    // Ray hit.
    Vector normal;
    Texture t;
    j->unitNormal(intersection, &normal);
    j->texture(intersection, &t);

    if ((depth < max_depth_) && (t.reflectiveness > 0.0)) {
      // Recursive cast reflection ray.
      Ray reflection(intersection, normal);
      reflection.origin += reflection.direction*kEpsilon;
      Color color2; // Will be set to color of reflection ray.
      cast_ray(reflection, depth + 1, &color2);
      // Texture color is alpha blend of reflection ray color and object color.
      t.color = color2*t.reflectiveness + t.color*(1.0 - t.reflectiveness);
    }

    illuminate(intersection, normal, t, color);
  }
}

// Convenience function to create a lib png pixel,
// 255 is maximum intensity. Simply round down for now.
png::rgb_pixel Scene::colorToPixel(const Color &c) {
  // Add kEpsilon to ensure 255.0 is safely rounded down to 255 not 254
  return png::rgb_pixel(std::min(c[0], 255.0 + kEpsilon),
                        std::min(c[1], 255.0 + kEpsilon),
                        std::min(c[2], 255.0 + kEpsilon));
}

void Scene::render(int start_line, int end_line) {
  const int super_samples = camera_->super_samples;
  const int jitter_samples = camera_->jitter_samples;
  boost::minstd_rand generator(108u);
  boost::uniform_01<> distribution;
  boost::variate_generator<boost::minstd_rand&, boost::uniform_01<> >
    random(generator, distribution);
  assert(end_line < camera_->size);

  for (size_t y = start_line; y <= end_line; ++y) {
//xxx    std::cout << "Line " << y + 1 << "/" << image_.get_height() << std::endl;
    for (size_t x = 0; x < image_.get_width(); ++x) {
      Vector focus = camera_->top_left;
      focus += camera_->x_increment*x + camera_->y_increment*y;
      Color c;

      if (!super_samples) {
        // No super sampling
        Ray r(camera_->origin, focus - camera_->origin);
        cast_ray(r, 0, &c);
      } else if (super_samples > 0) {
        // Super sample by regular subdividing
        // Alternatively could recursively subdivide until stability or 
        // maximum depth is reached.
        c = create_color(0.0, 0.0, 0.0);
        for (int i = 0; i < super_samples; ++i) {
          for (int j = 0; j < super_samples; ++j) {
            Vector super_focus = focus;
            super_focus += camera_->x_increment*i/super_samples;
            super_focus += camera_->y_increment*j/super_samples;
            if (!jitter_samples) {
              Ray r(camera_->origin, super_focus - camera_->origin);
              Color color;
              cast_ray(r, 0, &color);
              c += color;
            } else {
              // Super sample by random jittering
              for (int k = 0; k < jitter_samples*jitter_samples; ++k) {
                Vector jitter_focus = super_focus;
                // Note: factoring division out of loop into a uniform_real
                // random distribution doesn't improve performance
                jitter_focus += camera_->x_increment*random()/super_samples;
                jitter_focus += camera_->y_increment*random()/super_samples;
                Ray r(camera_->origin, jitter_focus - camera_->origin);
                Color color;
                cast_ray(r, 0, &color);
                c += color;
              }
            }
          }
        }
        if (super_samples) {
          c /= super_samples*super_samples;
        }
        if (jitter_samples) {
          c /= jitter_samples*jitter_samples;
        }
      }
      image_[y][x] = colorToPixel(c);
    }
  }
}

void Scene::render() {
#ifdef FUTURE
    // Use 16 threads on systems with C++11 support.
    // Can't yet rely on compiler support for automatically scheduling jobs.
    // e.g. on gcc 4.5.2 std::async schedules jobs consecutively rather than 
    // from a thread pool, or even just concurrently.
    const int jobs = std::min<int>(16, camera_->size - 1);
    std::vector<std::future<void>> futures;
    assert(camera_->size > jobs);
    for (int i = 0; i < jobs; ++ i) {
      const int start = i * camera_->size / jobs;
      const int end = (i + 1) * camera_->size / jobs;
      boost::function<void ()> 
        render_block(boost::bind(&Scene::render, this, start, end - 1));
      futures.push_back(std::async(std::launch::async, render_block));
    }
    for (size_t i = 0; i < futures.size(); ++i) {
      futures[i].get();
    }
#else
    render(0, camera_->size - 1);
#endif
}

void Scene::write(const std::string &filename) {
  // TODO:sanders check this throws an exception on error, e.g. out of space.
  image_.write(filename.c_str());
}

// Concrete class for sphere objects in the scene
class Sphere : public Primitive {
 public:
  Sphere(TextureFunction texture_function,
         const Vector &location,
         double radius);
  virtual bool intersect(const Ray &ray, Vector *intersection) const;
  virtual void unitNormal(const Vector &point, Vector *normal) const;

 private:
  double radius_;
  Vector location_;
};

Sphere::Sphere(TextureFunction texture_function,
               const Vector &location,
               double radius)
  :Primitive(texture_function),
   location_(location),
   radius_(radius) {
     check_zero(radius, i18n("Sphere radius must be positive"));
}

bool Sphere::intersect(const Ray &ray, Vector *intersection) const {
  // Nearest intersection of sphere and ray solved by quadratic equation.
  // http://en.wikipedia.org/wiki/Line%E2%80%93sphere_intersection
   // l is not necessarily unit vector, but must not be zero.
  const Vector l = ray.direction;
  const Vector c = location_ - ray.origin;
  const double l_dot_l = inner_prod(l, l);
  const double c_dot_l = inner_prod(c, l);
  const double c_dot_c = inner_prod(c, c);
  const double discriminant = c_dot_l*c_dot_l
                              - l_dot_l*(c_dot_c - radius_*radius_);
  if (discriminant < 0.0)
    return false;
  const double discriminant_root = sqrt(discriminant);
  const double t1 = c_dot_l + discriminant_root;
  const double t2 = c_dot_l - discriminant_root;
  double t;
  if (t1 < 0) {
    t = t2;
  } else if (t2 < 0) {
    t = t1;
  } else {
    t = std::min(t1, t2);
  }
  if (t < 0)
    return false;
  // Wikipedia page d = t/l_dot_l, delay division for efficiency
  // Can not divide by zero as ray direction must not be zero vector.
  *intersection = ray.origin + (t/l_dot_l)*l;
  return true;
}

void Sphere::unitNormal(const Vector &point, Vector *normal) const {
  *normal = (point - location_);
  // Can not divide by zero as sphere radius must be positive.
  *normal /= norm_2(*normal);
}

// Checkboard texture function
void checker_board(const Vector &point, Texture *texture) {
  texture->ka = 0.42;
  texture->kd = 1.22;
  texture->ks = 0.0;
  texture->phongAlpha = 11.0;
  texture->reflectiveness = 0.0;
#if 1 // for debugging recursive reflections
  texture->reflectiveness = 0.2;
#endif

  const double scale = 1.3;
  const int x = static_cast<int>(std::abs(point[0]/scale) + 0.5);
  const int y = static_cast<int>(std::abs((point[1]+0.5)/scale) + 0.5);
  if ((x % 2) ^ (y % 2)) {
    texture->color = create_vector(231.0, 213.0, 199.0);
  } else {
    texture->color = ublas::zero_vector<double>(3);
  }
}

// Shiny reflective ball texture function
void shiny_ball(const Vector &, Texture *texture) {
  texture->ka = 0.7;
  texture->kd = 0.90;
  texture->ks = 0.70;
  texture->phongAlpha = 7.0;
  texture->reflectiveness = 1.0;
  texture->color = create_color(255.1, 255.1, 255.1);
}

// Sky gradient background function
void sky_gradient(const Vector &v, Color *color) {
  // dark_blue to light_blue gradient
  Color dark_blue = create_color(152.0, 171.0, 247.0);
  Color light_blue = create_color(209.0, 217.0, 240.0);
  // Can not divide by zero as v must not be zero vector.
  double alpha = pow(1.0 - std::abs(v[2])/norm_2(v), 2.0);
  double blend = (std::max(0.75, alpha) - 0.75) * 4.0;
  *color = light_blue * blend + dark_blue * (1.0 - blend);
}

// Test two vectors for equality
bool equal(const Vector &a, const Vector &b) {
  if (a.size() != b.size())
    return false;

  // Fuzzy test, could use relative rather than absolute difference
  return norm_2(a - b) < 1.0/1000000.0;
}

void test() {
  // Test create_vector
  Vector o = ublas::zero_vector<double>(3);
  Vector x = create_vector(1.0, 0.0, 0.0);
  Vector y = create_vector(0.0, 1.0, 0.0);
  Vector z = create_vector(0.0, 0.0, 1.0);
  Vector a = create_vector(3.0, -3.0, 1.0);
  Vector b = create_vector(4.0, 9.0, 2.0);
  Vector c = create_vector(-15.0, -2.0, 39.0);
  assert(b[0] == 4.0);
  assert(b[1] == 9.0);
  assert(b[2] == 2.0);

  // Test equal
  assert(!equal(a, b));
  assert(equal(a, a));

  // Basic cross products;
  Vector result = cross_product(a, b);
  assert(equal(c, result));
  assert(equal(cross_product(x, y), z));
  assert(equal(cross_product(y, z), x));
  assert(equal(cross_product(z, x), y));

  // Intersection of line and sphere from different directions
  Sphere s1(checker_board, o, 1);
  Ray ray(y, o - y);
  Vector intersection;
  assert(s1.intersect(ray, &intersection));
  assert(equal(intersection, y));

  ray = Ray(z*8, o - z*2);
  assert(s1.intersect(ray, &intersection));
  assert(equal(intersection, z));

  ray = Ray(o, x + y + z);
  assert(s1.intersect(ray, &intersection));
  assert(equal(intersection, (x+y+z)*1/sqrt(3.0)));

  ray = Ray(2*z, x);
  assert(!s1.intersect(ray, &intersection));

  Sphere s2(checker_board, -z, 1);
  ray = Ray(z*8, o - z*2);
  assert(s2.intersect(ray, &intersection));
  assert(equal(intersection, o));

  // Basic camera setup
  Camera camera(y, o, z, 10, 5.0, 3, 2);
  assert(equal(camera.origin, y));
  assert(equal(camera.direction, o - y));
  assert(equal(camera.focus, o));
  assert(equal(camera.up, z));
  assert(equal(camera.left, x));
  assert(camera.size == 10);
  assert(camera.scale == 5.0);
  assert(camera.super_samples == 3);
  assert(camera.jitter_samples == 2);

  // Invalid scene
  bool exception_caught(false);
  try {
    // Invalid camera, position and focus are both x.
    Camera camera(x, x, z, 384, 6.0, 3, 1);
  }
  catch (bad_scene) {
    exception_caught = true;
  }
  assert(exception_caught);
}

} // namespace

int main(int, char**) {
  try {
    test();

    ObjectsPtr objects(new Objects);
    LightsPtr lights(new Lights);
#if 1
    // init objects
    //   big sphere looks like a plane
    const Vector s1_center(create_vector(0.0, 0.0, -1000000.0));
    objects->push_back(new Sphere(&checker_board, s1_center, 1000000.39));

    //   hovering sphere
    const Vector s2_center(create_vector(-7.0, -2.5, 2.15));
    objects->push_back(new Sphere(&shiny_ball, s2_center, 1.4));

    // init camera
    const Vector origin(create_vector(10.0, 3.5, 3.0)); // origin
    const Vector focus(create_vector(0.0, 0.0, 1.75)); // focal point
    const Vector up(create_vector(0.0, 0.0, 1.0)); // up hint
    const int size = 384*2; // output image height in pixels, big version
    const double scale = 6.0; // camera view port height in world coordinates
    const int super = 5; // linear average of n squared samples
    const int jitter = 1; // n squared random jitters per super sample
    CameraPtr camera(new Camera(origin, focus, up, size, scale, super, jitter));
#else
    // Alternative simple test scene
    const Vector s1_center(create_vector(0.0, 0.0, 0.0));
    const Vector origin(create_vector(0.0, 0.0, 200.0)); // origin
    const Vector focus(create_vector(0.0, 0.0, 0.0)); // focal point
    const Vector up(create_vector(0.0, 1.0, 0.0)); // up hint
    objects->push_back(new Sphere(checker_board, s1_center, 2.0));
    CameraPtr camera(new Camera(origin, focus, up, 384, 6.0, 3, 1));
#endif

    // init lights
    const double far = 1000000.0;
    const double pi = 4.0*atan(1.0);
    const double degrees = 2*pi/360;
    const double z_scale = far/0.535;
    lights->push_back(create_vector(-sin(18*degrees)*z_scale,
                                  cos(18*degrees)*z_scale, far));
#if 0 // for debugging multiple lights and reflection of a shadow on 
      // hovering ball
    const double z_scale2 = far/0.4;
    lights->push_back(create_vector(-sin(65*degrees)*z_scale2,
                                  cos(65*degrees)*z_scale2, far));
#endif

    Scene scene(sky_gradient, objects, lights, camera);
    const std::string filename("output.png");
    const clock_t start_time = clock(); // inaccurate, c++11 provides <chrono>
    scene.render();
    scene.write(filename);
    std::cout << "Rendering complete. lines " << camera->size
              << ", samples " << (camera->super_samples)*(camera->super_samples)
              << ", jitter " << (camera->jitter_samples)*(camera->jitter_samples)
                 // Note: Limited accuracy as clock() wraps on 32-bit systems 
                 // every 72 minutes. Also on Linux at least appears to count
                 // aggregate time used by all threads. So clock wraps in
                 // about 72/4 minutes on a 4 core system with 4 jobs running.
              << ", ms " << (clock() - start_time) * 1000 / CLOCKS_PER_SEC
#ifdef FUTURE
              << ", C++11 threading enabled"
#endif
              << std::endl;
    std::cout << "Output file \"" << filename << "\"" << std::endl;
  } 
  catch (bad_scene error) {
    std::cerr << i18n("Rendering failed, bad scene definition:") << std::endl;
    std::cerr << " " << error.what() << std::endl;
  }
  catch (std::bad_alloc error) {
    std::cerr << i18n("Rendering failed, out of memory:") << std::endl;
    std::cerr << " " << error.what() << std::endl;
  }
  return 0;
}

/*
  Next steps:
    simple constructors
  	  rejected: recommended by Google but no RAII
    follow google code guidelines
    Optimize for speed, consider using threads
      do some profiling.
      eliminate temporaries
      reduce sqrt including norm_2 calls
        might be better to normalize vectors before calling intersect
        unitNormal isn't needed in some situations, e.g. non-reflective
          points in the shade from all light sources (only ambient lighting).
    Double check coding style is compliant with Google standard
    Practice c++11
      in illuminate use new for (foreach)

    Render picture as it's being computed, using Qt?
      Progressive rendering
    Using cloud computing implement support for real time rendering
      using stream of rendering data

    More advanced ray tracing stuff
      CSG, bounding boxes,
      procedural objects (could have sphere with moon, recursively)
      unitNormal can also be abstracted to achieve bump mapping
      Translucency

  Done:
    Used const in more places
    Removed some superflous methods and variables
    Phong highlighting
    In scene::render() possibly size, and scale should be factored into Camera
    Abstract texture as its own object, add setTexture to primitive
      also reflectiveness could be part of texture function,
    Double rather than boolean reflection setting
    Multiple lights
    Recursive reflection
    Super sampling, uniform, and random
    Tried to inline texture and background function, but no speed improvement
    Relying on rvo seemed to slow the code down by 0.1%
    Considered static polymorphism using curiously recursive pattern
      rejected: doesn't support for containers of heterogeneous objects
    Line breaks at 80
    Fuzzy equal
*/
