All:
	g++ -c -std=c++0x -O2 raytracer.cc `libpng-config --cflags`
	g++ -o raytracer raytracer.o `libpng-config --ldflags` -lpthread

clean:
	rm raytracer
	rm raytracer.o